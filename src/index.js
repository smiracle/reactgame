import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import * as Redux from 'redux';
import * as ReactRedux from 'react-redux';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import Actions from './Actions';
import LocationData from './LocationData.json';
import ItemDefinitions from './ItemDefinitionData.json';
import _ from 'lodash';

var defaultState = {
    currentLocation: 'meadow',
    focus: 'nothing',
    latestAction: '',
    locations: LocationData,
    items: generateInitialItemsData(),
    itemDefinitions: Object.assign({}, ItemDefinitions),
    playerInventory: []
};

function includedInArray (toFind, arr) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === toFind) {
            return true;
        }
    }
    return false;
}

function generateInitialItemsData()
{
    var items = {};
    for( var itemKey in ItemDefinitions)
    {
        for (var locationKey in LocationData)
        {
            if(includedInArray(itemKey, LocationData[locationKey].inventory))
            {
                // Deep copy:
                var newItem = JSON.parse(JSON.stringify(ItemDefinitions[itemKey]));
                // This doesn't work because it is a shallow copy:
                // Object.assign({}, ItemDefinitions[itemKey]);
                newItem.location = Object.assign(locationKey);
                if(newItem.location !== 'player')
                {
                    var removedElement = _.remove(newItem.availableActions, function(element) {
                        return element === "Drop";
                    });
                }
                // Setting the item keys to match the definitions for now, but may want to refactor this later in favor of unique keys
                newItem.type = itemKey;
                items[itemKey] = newItem;
            }
        }

    }
    return items;
}

function reducer(state = defaultState, action) {
    var _locations = Object.assign({}, state.locations);
    var _locationInventory = Object.assign(_locations[state.currentLocation].inventory);
    var _playerInventory = Object.assign([],state.playerInventory);

    switch (action.type) {
        case Actions.MOVE:
            return Object.assign({}, state, { currentLocation: action.targetLocation, focus: "nothing", latestAction: 'move ' + action.targetLocation})
        case Actions.FOCUS:
            return Object.assign({}, state, { focus: action.focus }, {latestAction: 'focus ' + action.focus})
        case Actions.TAKE:
             // Remove the item from location if it exists
             var itemLabel = _.remove(_locationInventory, function(element) {
                return element === state.focus;
             });

             if(itemLabel)
             {
                // Put the item in the player's inventory
                _playerInventory = _playerInventory.concat(itemLabel);
                _locations[state.currentLocation].inventory = _locationInventory;

                // If the item is droppable, add the drop command
                if(_.includes(state.itemDefinitions[itemLabel].availableActions, 'Drop') && !_.includes(state.items[itemLabel].availableActions, 'Drop'))
                {
                    state.items[itemLabel].availableActions = state.items[itemLabel].availableActions.concat('Drop');
                }

                removeCommand(state, itemLabel, 'Take');
            }

            // Update the item's location variable to reflect this change
            state.items[itemLabel].location = 'player';
            return Object.assign({}, state, { locations: _locations, playerInventory: _playerInventory, focus: "nothing", latestAction: 'take ' + state.focus })
        case Actions.DROP:
            // Remove the item from player's inventory
            var itemLabel = _.remove(_playerInventory, function(element) {
                return element === state.focus;
             });

            // Put the item in the location's inventory
            _locationInventory = _locationInventory.concat(itemLabel);
            _locations[state.currentLocation].inventory = _locationInventory;

            // Update the item's location variable to reflect this change
            state.items[itemLabel].location = _locations[state.currentLocation].name;
            
            removeCommand(state, itemLabel, 'Drop');
            addCommand(state, itemLabel, 'Take');
            return Object.assign({}, state, { locations: _locations, playerInventory: _playerInventory, focus: "nothing", latestAction: 'drop ' + state.focus  })
        case Actions.READ:
            return Object.assign({}, state, { focus: state.focus }, { latestAction: 'read ' + state.focus })
        case Actions.NOTHING:
        default:
            return Object.assign({}, state, { focus: 'nothing' }, { latestAction: 'unfocus ' + state.focus })
    }
}

let store = Redux.createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
    <ReactRedux.Provider store={store}>
        <BrowserRouter>
            <React.Fragment>
                <Route exact path="/" component={App} />
            </React.Fragment>
        </BrowserRouter>
    </ReactRedux.Provider>
    , document.getElementById('root'));
registerServiceWorker();

function commandSorter(a, b) {
    var x = a.toLowerCase();
    var y = b.toLowerCase();
    if (x === "nothing") {
        return 1;
    }
    if(y ==="nothing"){
        return -1;
    }
    if (x < y) {
        return -1;
    }
    else if (x > y) {
        return 1;
    }
    else {
        return 0;
    }
}

// Uses the command sorter function to sort commands, puts "nothing" last
function sortCommands(state, itemLabel) {
    // state.items[itemLabel].availableActions = state.items[itemLabel].availableActions.sort();
    state.items[itemLabel].availableActions = state.items[itemLabel].availableActions.sort(commandSorter);
}

// Add a command if the item definition allows it
function addCommand(state, itemLabel, commandToAdd) {
    if (_.includes(state.itemDefinitions[itemLabel].availableActions, commandToAdd) && !_.includes(state.items[itemLabel].availableActions, commandToAdd)) {
        state.items[itemLabel].availableActions = state.items[itemLabel].availableActions.concat(commandToAdd);
    }
    sortCommands(state, itemLabel);
}

// Removes a command if present in the array
function removeCommand(state, itemLabel, commandToRemove) {
    if (_.includes(state.items[itemLabel].availableActions, commandToRemove)) {
        _.remove(state.items[itemLabel].availableActions, function (element) {
            return element === commandToRemove;
        });
    }
    sortCommands(state, itemLabel);
}

