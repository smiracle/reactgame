import React from 'react';
import { StringToAction } from '../Actions';
import './App.css';

class FocusedItemAction extends React.Component {
    render() {
        return (
            <span>
                <span onClick={() => this.props.onClick(this.props.name, StringToAction(this.props.name))} className="link" >
                    {this.props.name}
                </span>
                &nbsp;
            </span>
        );
    }
}

export default FocusedItemAction;