import React from 'react';

class LocationInventoryItem extends React.Component {
    render() {
        return (
            <React.Fragment>
                {this.props.isLastItem && this.props.numItems > 0 ? " and a " : " a "}
                <span onClick={() => this.props.onClick(this.props.label)} className="link" >
                    {this.props.name}
                </span>
            </React.Fragment >);
    }
}

export default LocationInventoryItem;