import React from 'react';

class Exit extends React.Component {
    render() {
        return (
            <div onClick={() => this.props.onClick(this.props.targetLocation)} className="link marginBottom15" >
                {this.props.label}
            </div>);
    }
}

export default Exit;