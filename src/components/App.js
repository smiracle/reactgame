import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './App.css';
import ExitList from './ExitList';
import InventoryList from './InventoryList';
import FocusedItem from './FocusedItem';
import Actions from '../Actions';

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    focus: state.focus,
    latestAction: state.latestAction,
    playerInventory: state.playerInventory,
    locations: state.locations,
    items: state.items,
    itemDefinitions: state.itemDefinitions,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    onClickExitLink: (targetLocation) => {
      dispatch({ type: Actions.MOVE, targetLocation });
    },
    onClickItemFocusLink: (focus) => {
      dispatch({ type: Actions.FOCUS, focus });
    },
    onClickItemActionLink: (focus, actionType) => {
      dispatch({ type: actionType, focus });
    },
  };
}

const App = connect(mapStateToProps, mapDispatchToProps)(
  class App extends React.Component {
    render() {
      var items = this.props.items;
      return (
        <div className="app">
          <div>
            <div className="locationNameHeader">{this.props.locations[this.props.currentLocation].name}</div>
            <div className="marginBottom15">{this.props.locations[this.props.currentLocation].description}</div>
            {this.props.focus === "nothing" ?
              <div>
                <InventoryList inventory={this.props.locations[this.props.currentLocation].inventory} onClick={this.props.onClickItemFocusLink} items={items} modifier={"location"} />
                <InventoryList inventory={this.props.playerInventory} onClick={this.props.onClickItemFocusLink} items={items} modifier={"player"} />
              </div>
              :
              <FocusedItem items={items} itemDefinitions={this.props.itemDefinitions} focusedItemData={items[this.props.focus]} currentLocationData={this.props.currentLocation} playerInventory={this.props.playerInventory} latestAction={this.props.latestAction} onClick={this.props.onClickItemActionLink} />
            }
            <ExitList exits={this.props.locations[this.props.currentLocation].exits} onClick={this.props.onClickExitLink} />
          </div>
        </div>
      );
    };
  });

export default withRouter(connect(() => mapStateToProps, mapDispatchToProps)(App));
