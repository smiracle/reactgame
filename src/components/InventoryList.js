import React from 'react';
import InventoryItem from './InventoryItem';

class InventoryList extends React.Component {

    getStartModifierString(mod) {
        var modifier = "";
        if (mod === "location") {
            if (this.props.inventory.length > 0) {
                modifier = "There is "
            }
        }
        else if (mod === "player") {
            modifier = "You are carrying "
        }
        return modifier;
    }

    getEndModifierString(mod){
        var modifier = ".";
        if(mod === "location"){
            modifier = " here.";
        }
        return modifier;
    }

    render() {
        return (
            this.props.inventory.length > 0 ?
            <div className="marginBottom15">
                {this.getStartModifierString(this.props.modifier)}
                {this.props.inventory.map((inventoryItem, index) => <InventoryItem key={index} numItems={this.props.length} isLastItem={index === this.props.inventory.length - 1} label={inventoryItem} name={inventoryItem} onClick={this.props.onClick} />)}
                {this.getEndModifierString(this.props.modifier)}
            </div>
            :
            <div />
        );
    }
}

export default InventoryList;