import React from 'react';
import FocusedItemAction from './FocusedItemAction';
import _ from 'lodash';

class FocusedItem extends React.Component {

    getFocusedItemDescription(latestAction, itemData)
    {
        if(itemData.readDescription !== null && latestAction === 'read ' + itemData.name)
        {
            return <div>
                <p>{itemData.readDescription}</p>
                <p>What do you want to do with the {itemData.name}?</p>
            </div>;
        }
        else 
        {
            return <p>What do you want to do with the {itemData.name}?</p>;
        }
    }

    render() {
        return (
            <div className="marginBottom15">
                {this.getFocusedItemDescription(this.props.latestAction, this.props.focusedItemData)}
                {(this.props.focusedItemData.availableActions).map((action, index) => 
                    <FocusedItemAction key={index} label={action} name={action} onClick={this.props.onClick} />)}
                {/* {(this.props.items[this.props.focusedItemData.name].availableActions).map((action, index) => 
                    <FocusedItemAction key={index} label={action} name={action} onClick={this.props.onClick} />)} */}
            </div>
        );
    }
}

export default FocusedItem;