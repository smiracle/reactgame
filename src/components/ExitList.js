import React from 'react';
import Exit from './Exit';

class ExitList extends React.Component {
    render() {
        return (
            this.props.exits.map((exit, index) => <Exit key={index} label={Object.entries(exit)[0][1]} targetLocation={Object.entries(exit)[0][0]} onClick={this.props.onClick} />)
        );
    }
}

export default ExitList;