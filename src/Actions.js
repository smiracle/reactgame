const Actions = Object.freeze({
    "NOTHING": 1,
    "MOVE": 2,
    "FOCUS": 3,
    "TAKE": 4,
    "DROP": 5,
    "READ": 6,
});

export const StringToAction = (str) => {
    switch (str) {
        case "Move":
            return Actions.MOVE;
        case "Focus":
            return Actions.FOCUS;
        case "Take":
            return Actions.TAKE;
        case "Drop":
            return Actions.DROP;
        case "Read":
            return Actions.READ;
        default:
            return Actions.NOTHING;
    }
}

export default Actions;